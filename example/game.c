#include <string.h>

#include "asset.h"
#include "font.h"
#include "platform.h"

typedef struct {
    uint32_t screen_w, screen_h;
    int bx, by;
    int vx, vy;
    int mx, my;
    bitmap_t img_ball, img_paddle;
    tilemap_t map;
} game_state_t;

game_state_t *game_init(platform_api_t *platform, platform_params_t *params) {
    game_state_t *game = platform->alloc(sizeof(game_state_t));

    game->screen_w = params->screen_w;
    game->screen_h = params->screen_h;

    asset_load_image(platform, "example/data/ball.dat", &game->img_ball);
    asset_load_image(platform, "example/data/paddle.dat", &game->img_paddle);
    asset_load_tilemap(platform, "example/data/testmap.dat", &game->map);
    asset_load_image(platform, "example/data/tiles_testmap_8x8.dat", &game->map.tileset);
    game->bx = 80;
    game->by = 60;
    game->vx = 1;
    game->vy = 1;
    game->mx = 64;
    game->my = 180;
    return game;
}

void game_term(game_state_t *game) {
}

void game_tick(game_state_t *game, input_state_t *input, uint8_t *framebuffer) {
    bitmap_t screen = {.width = game->screen_w,
                       .height = game->screen_h,
                       .format = BITMAP_FORMAT_RGB565,
                       .data = framebuffer};

    bitmap_fill(&screen, RGB565(3, 6, 5));

    if (input->up) {
        game->my -= 2;
    }
    if (input->down) {
        game->my += 2;
    }
    if (input->left) {
        game->mx -= 2;
    }
    if (input->right) {
        game->mx += 2;
    }
    game->mx = CLAMP(game->mx, 0, game->map.width * 8 - screen.width);
    game->my = CLAMP(game->my, 0, game->map.height * 8 - screen.height);

    game->bx += game->vx;
    game->by += game->vy;

    if (game->bx < 12) {
        game->bx = 12;
        game->vx *= -1;
    }
    if (game->bx > game->screen_w - 11) {
        game->bx = game->screen_w - 11;
        game->vx *= -1;
    }
    if (game->by < 5) {
        game->by = 5;
        game->vy *= -1;
    }
    if (game->by > game->screen_h - 5) {
        game->by = game->screen_h - 5;
        game->vy *= -1;
    }

    // "AI"
    int ey = CLAMP(game->by, 15, game->screen_h - 15);

    // draw map
    tilemap_render(&game->map, &screen, 0, game->mx, game->my);

    // draw ball, player and enemy
    bitmap_blit_full(&screen, &game->img_ball, game->bx - 5, game->by - 5);
    bitmap_blit_full(&screen, &game->img_paddle, game->screen_w - 8, ey - 15);
    bitmap_blit_full(&screen, &game->img_paddle, 2, ey - 15);

    font_print(&screen, font_system(), 12, 8, "###>----: Test Pong Game :----<###");
}

void game_audio(game_state_t *game, void *audio_buf, size_t audio_buf_len) {
    // TODO
}
