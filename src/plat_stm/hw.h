#pragma once

#include <stdint.h>

void hw_init(void);
void hw_clock_init(void);
void hw_wait_vblank(void);
void hw_lcd_init(uint8_t *framebuffer, uint32_t fb_w, uint32_t fb_h);
