# vim: set ft=make:

OPT?=s
ARCHFLAGS:= -mthumb -march=armv7e-m -mfpu=fpv4-sp-d16 -mfloat-abi=hard
PLATFLAGS:= $(ARCHFLAGS) -DSTM32F746xx -DTS_MULTI_TOUCH_SUPPORTED -ffunction-sections -fdata-sections
LDFLAGS = $(ARCHFLAGS) -lm -Lsrc/plat_stm --static --specs=nosys.specs -Wl,--gc-sections -Tstm32f746ngh.ld

CC:=arm-none-eabi-gcc
AS:=arm-none-eabi-as
GDB:=arm-none-eabi-gdb
OBJCOPY:=arm-none-eabi-objcopy
OBJDUMP:=arm-none-eabi-objdump

DEBUGARGS=-x misc/gdb
BINEXT=.elf


# STM32Cube HAL peripheral driver library
HAL_SRCS:= \
    src/lib/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal.c \
    src/lib/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_cortex.c \
    src/lib/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_pwr_ex.c \
    src/lib/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_rcc.c \
    src/lib/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_rcc_ex.c \
    src/lib/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_gpio.c \
    src/lib/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_i2c.c \
    src/lib/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_dma.c \
    src/lib/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_ltdc.c \
    src/lib/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_sd.c \
    src/lib/STM32F7xx_HAL_Driver/Src/stm32f7xx_hal_sai.c \
    src/lib/STM32F7xx_HAL_Driver/Src/stm32f7xx_ll_sdmmc.c \

# STM32F746G Discovery board BSP
BSP_SRCS:= \
    src/lib/bsp/STM32746G-Discovery/stm32746g_discovery.c \
    src/lib/bsp/STM32746G-Discovery/stm32746g_discovery_audio.c \
    src/lib/bsp/STM32746G-Discovery/stm32746g_discovery_sd.c \
    src/lib/bsp/STM32746G-Discovery/stm32746g_discovery_ts.c \
    src/lib/bsp/Components/ft5336/ft5336.c \
    src/lib/bsp/Components/wm8994/wm8994.c \

# FatFs library
FATFS_SRCS:= \
    src/lib/fatfs/ff.c \
    src/lib/fatfs/ffsystem.c \
    src/lib/fatfs/ff_gen_drv.c \
    src/lib/fatfs/diskio.c \
    src/lib/fatfs/option/ccsbcs.c \

# Platform and startup code
SRCS+= \
    src/plat_stm/main.c \
    src/plat_stm/hw.c \
    src/plat_stm/system_stm32f7xx.c \
    src/plat_stm/stm32f7xx_hal_msp.c \
    src/plat_stm/sd_diskio.c \
    $(BSP_SRCS) \
    $(HAL_SRCS) \
    $(FATFS_SRCS) \

# Assembly sources
ASRCS:= \
    src/plat_stm/startup_stm32f746xx.s \

INCS:= \
    -Isrc/lib/bsp/STM32746G-Discovery \
    -Isrc/lib/STM32F7xx_HAL_Driver/Inc \
    -Isrc/lib/cmsis-stm32/stm32f7xx/Include \
    -Isrc/lib/cmsis/Include \
    -Isrc/lib/fatfs \
    -Isrc/plat_stm \
    -Isrc/engine \

PLATTARGETS := game.lst game.bin
