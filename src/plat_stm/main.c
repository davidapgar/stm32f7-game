#include <math.h>
#include <stdlib.h>
#include <string.h>

#include "sd_diskio.h"
#include "stm32746g_discovery_audio.h"
#include "stm32746g_discovery_lcd.h"
#include "stm32746g_discovery_ts.h"

#include "common.h"
#include "input.h"
#include "platform.h"

#include "hw.h"

#define FP_POOL_SIZE 2

typedef struct {
    FIL fp;
    bool inuse;
} fp_item_t;

#define LCD_SCALE (2)
#define SCREEN_W (160)
#define SCREEN_H (120)
#define AUDIO_SAMPLE_RATE (8000)
#define AUDIO_SAMPLES (4 * 1024)

typedef enum {
    NONE,
    HALF,
    FULL,
} dma_bufpos_t;

typedef struct main_priv_t {
    uint8_t framebuffer[SCREEN_W * LCD_SCALE * SCREEN_H * LCD_SCALE * 2];
    uint8_t backbuffer[SCREEN_W * SCREEN_H * 2];
    uint16_t audiobuf[AUDIO_SAMPLES];
    uint16_t audiobuf2[AUDIO_SAMPLES / 4];
    size_t total_allocs;
    struct game_state_t *game_state;
    volatile dma_bufpos_t bufpos;

    char SDPath[4];
    FATFS SDFatFs;
    fp_item_t fp_pool[FP_POOL_SIZE];
} main_priv_t;
main_priv_t main_priv;

extern struct game_state_t *game_init(platform_api_t *platform, platform_params_t *params);
extern void game_term(struct game_state_t *game);
extern void game_tick(struct game_state_t *game, input_state_t *input, uint8_t *framebuffer);
extern void game_audio(struct game_state_t *game, void *buf, size_t len);

float get_ticks() {
    return HAL_GetTick();
}

int fp_pool_alloc() {
    for (int i = 0; i < FP_POOL_SIZE; i++) {
        if (!main_priv.fp_pool[i].inuse) {
            main_priv.fp_pool[i].inuse = true;
            return i + 1;
        }
    }
    ASSERT_MSG(false, "Cannot alloc fp item");
}

void fp_pool_free(int idx) {
    main_priv.fp_pool[idx - 1].inuse = false;
}

void *platform_mem_alloc(size_t size) {
    main_priv.total_allocs += size;
    return calloc(size, 1);
}

void platform_mem_free(void *ptr) {
    free(ptr);
}

platform_handle_t platform_file_open(const char *filename, bool write) {
    int idx = fp_pool_alloc();
    FIL *fp = &main_priv.fp_pool[idx - 1].fp;
    FRESULT res = f_open(fp, filename, write ? FA_CREATE_ALWAYS | FA_WRITE : FA_READ);
    ASSERT_MSG(res == FR_OK, "Cannot open asset file %s", filename);
    return (platform_handle_t)idx;
}

size_t platform_file_read(platform_handle_t h, size_t n, void *b) {
    ASSERT(h);
    FIL *fp = &main_priv.fp_pool[(int)h - 1].fp;
    size_t nread = 0;
    FRESULT res = f_read(fp, b, n, &nread);
    ASSERT_MSG(res == FR_OK, "Cannot read asset file");
    return nread;
}

size_t platform_file_write(platform_handle_t h, size_t n, void *b) {
    ASSERT(h);
    FIL *fp = &main_priv.fp_pool[(int)h - 1].fp;
    size_t nwritten;
    FRESULT res = f_write(fp, b, n, &nwritten);
    ASSERT_MSG(res == FR_OK, "Cannot write asset file");
    return nwritten;
}

void platform_file_close(platform_handle_t h) {
    FIL *fp = &main_priv.fp_pool[(int)h - 1].fp;
    FRESULT res = f_close(fp);
    fp_pool_free((int)h);
    ASSERT_MSG(res == FR_OK, "Cannot close asset file");
}

static void update_input_button(input_state_t *input, uint16_t x, uint16_t y) {
    if (x < 100) {
        input->left = true;
    }
    if (x > 480 - 100) {
        input->right = true;
    }
    if (y < 100) {
        input->up = true;
    }
    if (y > 272 - 100) {
        input->down = true;
    }
}

void process_input(input_state_t *input) {
    TS_StateTypeDef ts_state = {};
    BSP_TS_GetState(&ts_state);
    input->left = input->right = input->up = input->down = false;
    for (int touch = 0; touch < ts_state.touchDetected; touch++) {
        uint16_t tx = ts_state.touchX[touch];
        uint16_t ty = ts_state.touchY[touch];
        update_input_button(input, tx, ty);
    }
}

void blit_scale2(uint8_t *dst, uint8_t *src, uint32_t w, uint32_t h) {
    uint16_t *sh = (uint16_t *)src;
    uint16_t *dh = (uint16_t *)dst;
    for (int j = h - 1; j >= 0; j--) {
        for (int i = w - 1; i >= 0; i--) {
            uint16_t s = *(sh + (j * w) + i);
            *((dh + ((j * 2 + 1) * w * 2) + i * 2 + 0)) = s;
            *((dh + ((j * 2 + 0) * w * 2) + i * 2 + 1)) = s;
            *((dh + ((j * 2 + 0) * w * 2) + i * 2 + 0)) = s;
            *((dh + ((j * 2 + 1) * w * 2) + i * 2 + 1)) = s;
        }
    }
}

void BSP_AUDIO_OUT_TransferComplete_CallBack(void) {
    ASSERT(main_priv.bufpos == NONE);
    main_priv.bufpos = FULL;
}

void BSP_AUDIO_OUT_HalfTransfer_CallBack(void) {
    ASSERT(main_priv.bufpos == NONE);
    main_priv.bufpos = HALF;
}

void audio_update(void) {
    if (main_priv.bufpos == NONE) {
        return;
    }
    game_audio(main_priv.game_state, main_priv.audiobuf2, sizeof(main_priv.audiobuf2));
    uint32_t ofs = (main_priv.bufpos == FULL) ? AUDIO_SAMPLES / 2 : 0;
    // FIXME something off-by-two in STM code (samples/s vs samples/channel/s maybe?)
    for (int i = 0; i < AUDIO_SAMPLES / 4; i++) {
        main_priv.audiobuf[ofs + i * 2] = main_priv.audiobuf2[i];
        main_priv.audiobuf[ofs + i * 2 + 1] = main_priv.audiobuf2[i];
    }
    main_priv.bufpos = NONE;
}

int main() {
    uint8_t status;
    hw_init();
    hw_clock_init();
    hw_lcd_init(main_priv.framebuffer, SCREEN_W * LCD_SCALE, SCREEN_H * LCD_SCALE);

    platform_params_t params = {
        .screen_w = SCREEN_W,
        .screen_h = SCREEN_H,
        .audio_rate = AUDIO_SAMPLE_RATE,
    };

    status = BSP_AUDIO_OUT_Init(OUTPUT_DEVICE_BOTH, 20, AUDIO_SAMPLE_RATE);
    ASSERT(status == AUDIO_OK);

    status = BSP_TS_Init(480, 272);
    ASSERT(status == TS_OK);

    ASSERT(FATFS_LinkDriver(&SD_Driver, main_priv.SDPath) == 0);
    ASSERT(f_mount(&main_priv.SDFatFs, (TCHAR const *)main_priv.SDPath, 0) == FR_OK);

    platform_api_t platform = {
        platform_mem_alloc, platform_mem_free,   platform_file_open,
        platform_file_read, platform_file_write, platform_file_close,
    };

    game_api_t game = {
        game_init,
        game_term,
        game_tick,
        game_audio,
    };

    main_priv.game_state = game.init(&platform, &params);
    BSP_AUDIO_OUT_Play(main_priv.audiobuf, sizeof(main_priv.audiobuf));

    input_state_t input = {};
    float last_tick = get_ticks() - 16;
    while (1) {
        float now_tick = get_ticks();
        input.dt = (now_tick - last_tick) / 1000.0f;
        last_tick = now_tick;
        process_input(&input);
        game.tick(main_priv.game_state, &input, main_priv.backbuffer);

        audio_update();
        hw_wait_vblank();
        blit_scale2(main_priv.framebuffer, main_priv.backbuffer, SCREEN_W, SCREEN_H);
    }
}
