#ifndef __SD_DISKIO_H
#define __SD_DISKIO_H

#include "ff_gen_drv.h"
#include "stm32746g_discovery_sd.h"

extern const Diskio_drvTypeDef SD_Driver;

#endif
