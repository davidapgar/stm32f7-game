# vim: set ft=make:

OPT?=g
SDL_CFLAGS=$(shell sdl2-config --cflags)
SDL_LDFLAGS=$(shell sdl2-config --libs)
PLATFLAGS:= $(SDL_CFLAGS) -fPIC -DPLAT_SDL=1 -DPLAT_DEBUG_PRINT=1
PLATFLAGS+=-fsanitize=address
LDFLAGS:=-lm -ldl -lpthread $(SDL_LDFLAGS) $(PLATFLAGS)
GDB=gdb

SRCS += \
    src/plat_sdl/main.c

INCS:=\
    -Isrc/lib/jo_gif \
    -Isrc/plat_sdl \
    -Isrc/engine
