#include <SDL2/SDL.h>
#include <libgen.h>
#include <string.h>

#include "common.h"
#include "input.h"
#include "platform.h"

#include "jo_gif.h"

#define SCREEN_W (160)
#define SCREEN_H (120)
#define AUDIO_SAMPLE_RATE (8000)

const int JOY_DEADZONE = 8000;
const float TICK_INTERVAL = 1000 / 60.;

typedef struct main_priv_t {
    SDL_Window *Window;
    float start_time, next_time;
    uint32_t framecount;
    size_t total_allocs;
    bool gif_recording;
    char *base_dir;
} main_priv_t;
main_priv_t main_priv;

extern struct game_state_t *game_init(platform_api_t *platform, platform_params_t *params);
extern void game_term(struct game_state_t *game);
extern void game_tick(struct game_state_t *game, input_state_t *input, uint8_t *framebuffer);
extern void game_audio(struct game_state_t *game, void *buf, size_t len);

static float time_left(float next_time) {
    float now = SDL_GetTicks();
    return (next_time <= now) ? 0 : (next_time - now);
}

static void wait_for_next_frame() {
    SDL_Delay(time_left(main_priv.next_time));
    float cur_time = (float)SDL_GetTicks();
    if (cur_time > (main_priv.start_time + 250.0f)) {
        char titlebuf[32];
        float fps = (main_priv.framecount / ((cur_time - main_priv.start_time) / 1000.0f));
        char *gifrec = main_priv.gif_recording ? " gif" : "";
        snprintf(titlebuf, sizeof(titlebuf), "Game [%0.1ffps%s]", (double)fps, gifrec);
        SDL_SetWindowTitle(main_priv.Window, titlebuf);
        main_priv.start_time = cur_time;
        main_priv.framecount = 0;
    }
    main_priv.next_time += TICK_INTERVAL;
    main_priv.framecount++;
}

static void update_input_key(input_state_t *input, uint32_t key, bool pressed) {
    switch (key) {
        case SDLK_UP:
            input->up = pressed;
            break;
        case SDLK_DOWN:
            input->down = pressed;
            break;
        case SDLK_LEFT:
            input->left = pressed;
            break;
        case SDLK_RIGHT:
            input->right = pressed;
            break;
        case SDLK_z:
            input->a = pressed;
            break;
        case SDLK_x:
            input->b = pressed;
            break;
        case SDLK_a:
            input->c = pressed;
            break;
        case SDLK_s:
            input->d = pressed;
            break;
        case SDLK_ESCAPE:
            input->menu = pressed;
            break;
        case SDLK_F7:
            if (!pressed) {
                main_priv.gif_recording = !main_priv.gif_recording;
            }
            break;
    }
}

static void update_input_joy(input_state_t *input, SDL_JoystickID id, uint8_t axis, int16_t value) {
    if (id != 0)
        return;

    bool pressed = ABS(value) > JOY_DEADZONE;
    if (axis == 0) {
        input->left = pressed && (value < 0);
        input->right = pressed && (value > 0);
    }
    if (axis == 1) {
        input->up = pressed && (value < 0);
        input->down = pressed && (value > 0);
    }
}

static void update_input_but(input_state_t *input, SDL_JoystickID id, uint8_t button,
                             bool pressed) {
    if (id != 0)
        return;
    switch (button) {
        case 0:
            input->a = pressed;
            break;
        case 1:
            input->b = pressed;
            break;
        case 2:
            input->c = pressed;
            break;
        case 3:
            input->d = pressed;
            break;
        case 7:
            input->menu = pressed;
            break;
    }
}

static void update_input_hat(input_state_t *input, SDL_JoystickID id, uint8_t hat, uint8_t value) {
    if (id != 0)
        return;
    switch (value) {
        case SDL_HAT_UP:
            input->up = true;
            break;
        case SDL_HAT_DOWN:
            input->down = true;
            break;
        case SDL_HAT_LEFT:
            input->left = true;
            break;
        case SDL_HAT_RIGHT:
            input->right = true;
            break;
    }
}

static void process_input(input_state_t *input) {
    SDL_Event event;
    while (SDL_PollEvent(&event)) {
        switch (event.type) {
            case SDL_QUIT:
                input->menu = true;
                break;

            case SDL_KEYDOWN:
            case SDL_KEYUP:
                update_input_key(input, event.key.keysym.sym, event.type == SDL_KEYDOWN);
                break;

            case SDL_JOYAXISMOTION:
                update_input_joy(input, event.jaxis.which, event.jaxis.axis, event.jaxis.value);
                break;

            case SDL_JOYBUTTONDOWN:
            case SDL_JOYBUTTONUP:
                update_input_but(input, event.jbutton.which, event.jbutton.button,
                                 event.type == SDL_JOYBUTTONDOWN);
                break;

            case SDL_JOYHATMOTION:
                update_input_hat(input, event.jhat.which, event.jhat.hat, event.jhat.value);
                break;
        }
    }
}

void *platform_mem_alloc(size_t size) {
    main_priv.total_allocs += size;
    return calloc(size, 1);
}

void platform_mem_free(void *ptr) {
    free(ptr);
}

platform_handle_t platform_file_open(const char *filename, bool write) {
    char fn[256] = {0};
    strncat(fn, main_priv.base_dir, sizeof(fn));
    strncat(fn, "/", sizeof(fn));
    strncat(fn, filename, sizeof(fn));

    FILE *f = fopen(fn, write ? "wb" : "rb");
    ASSERT_MSG(f, "Cannot open asset file %s", fn);
    return (platform_handle_t)f;
}

size_t platform_file_read(platform_handle_t h, size_t n, void *b) {
    ASSERT(h);
    return fread(b, 1, n, (FILE *)h);
}

size_t platform_file_write(platform_handle_t h, size_t n, void *b) {
    ASSERT(h);
    return fwrite(b, 1, n, (FILE *)h);
}

void platform_file_close(platform_handle_t h) {
    fclose((FILE *)h);
}

void audio_callback(void *userdata, uint8_t *stream, int len) {
    struct game_state_t *game_state = (struct game_state_t *)userdata;
    uint8_t buf[1024];
    ASSERT(len <= sizeof(buf));
    game_audio(game_state, buf, len);
    SDL_memcpy(stream, buf, len);
}

int main(int argc, char *argv[]) {
    SDL_Surface *screen;
    SDL_Joystick *game_controller = NULL;

    int pixscale = 3;
    platform_params_t params = {
        .screen_w = SCREEN_W,
        .screen_h = SCREEN_H,
        .audio_rate = AUDIO_SAMPLE_RATE,
    };

    // extract running directory name
    char *exename = strdup(argv[0]);
    main_priv.base_dir = strdup(dirname(exename));
    free(exename);

    platform_api_t platform = {
        platform_mem_alloc, platform_mem_free,   platform_file_open,
        platform_file_read, platform_file_write, platform_file_close,
    };
    game_api_t game = {
        game_init,
        game_term,
        game_tick,
        game_audio,
    };

    struct game_state_t *game_state = game.init(&platform, &params);

    SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO | SDL_INIT_JOYSTICK);

    if (SDL_NumJoysticks()) {
        game_controller = SDL_JoystickOpen(0);
        if (!game_controller) {
            printf("Warning: Unable to open game controller! SDL Error: %s\n", SDL_GetError());
        }
    }

    SDL_AudioSpec desiredSpec, obtainedSpec;
    desiredSpec.freq = AUDIO_SAMPLE_RATE;
    desiredSpec.format = AUDIO_S16SYS;
    desiredSpec.channels = 2;
    desiredSpec.samples = 256;
    desiredSpec.callback = audio_callback;
    desiredSpec.userdata = game_state;

    if (SDL_OpenAudio(&desiredSpec, &obtainedSpec) < 0) {
        printf("Error initiatising audio\n");
        exit(-1);
    }

    main_priv.Window = SDL_CreateWindow("", 0, 0, 0, 0, SDL_WINDOW_HIDDEN);
    SDL_SetWindowSize(main_priv.Window, params.screen_w * pixscale, params.screen_h * pixscale);
    SDL_ShowWindow(main_priv.Window);
    // SDL_SetWindowPosition(main_priv.Window, SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED);

    screen = SDL_GetWindowSurface(main_priv.Window);

    SDL_Surface *buffer, *gifframe;
    buffer = SDL_CreateRGBSurface(0, params.screen_w, params.screen_h, 16, 0, 0, 0, 0);
    ASSERT(buffer->format->BytesPerPixel == 2);

    printf("Allocated bytes after init: %ld\n", main_priv.total_allocs);

    input_state_t input = {};
    main_priv.start_time = main_priv.next_time = SDL_GetTicks() + TICK_INTERVAL;
    float last_tick = main_priv.start_time - TICK_INTERVAL;

    // gif recording support
    jo_gif_t gif = jo_gif_start("game.gif", params.screen_w, params.screen_h, 0, 32);
    // create a surface with appropriate RGBA byte ordering for jo_gif
    gifframe = SDL_CreateRGBSurface(0, params.screen_w, params.screen_h, 32, 0x000000ff, 0x0000ff00,
                                    0x00ff0000, 0xff000000);
    SDL_PauseAudio(0);

    while (!input.menu) {
        float now_tick = SDL_GetTicks();
        input.dt = (now_tick - last_tick) / 1000.0f;
        last_tick = now_tick;
        process_input(&input);

        game.tick(game_state, &input, (uint8_t *)buffer->pixels);

        SDL_BlitScaled(buffer, NULL, screen, NULL);
        SDL_UpdateWindowSurface(main_priv.Window);
        if (main_priv.gif_recording) {
            SDL_BlitSurface(buffer, NULL, gifframe, NULL);
            jo_gif_frame(&gif, (uint8_t *)gifframe->pixels, 2, false);
        }
        wait_for_next_frame();
    }
    game.term(game_state);

    jo_gif_end(&gif);

    SDL_CloseAudio();
    SDL_FreeSurface(gifframe);
    SDL_FreeSurface(screen);
    SDL_FreeSurface(buffer);
    SDL_DestroyWindow(main_priv.Window);
    if (game_controller) {
        SDL_JoystickClose(game_controller);
    }
    SDL_Quit();
    free(main_priv.base_dir);
    return EXIT_SUCCESS;
}
