#pragma once

#include <stddef.h>
#include <stdint.h>

#include "input.h"

typedef struct {
    uint32_t screen_w, screen_h;
    uint32_t audio_rate;
} platform_params_t;

// Asset handle used by platform_api_t file functions
typedef void *platform_handle_t;

typedef struct {
    // Allocate a block of `size` bytes and return a pointer to it
    void *(*alloc)(size_t size);
    // Free a block of memory allocated by `alloc`
    void (*free)(void *ptr);
    // Open an file by name for reading or writing, return an file handle
    platform_handle_t (*file_open)(const char *name, bool write);
    // Read `n` bytes from a given file into `buf`. Return number of bytes read
    size_t (*file_read)(platform_handle_t h, size_t n, void *buf);
    // Write `n` bytes from `buf` into a given file. Return number of bytes written
    size_t (*file_write)(platform_handle_t h, size_t n, void *buf);
    // Close the file referenced by the given handle
    void (*file_close)(platform_handle_t h);
} platform_api_t;

struct game_state_t;

typedef struct {
    struct game_state_t *(*init)(platform_api_t *platform, platform_params_t *params);
    void (*term)(struct game_state_t *game);
    void (*tick)(struct game_state_t *game, input_state_t *input, uint8_t *framebuffer);
    void (*audio)(struct game_state_t *game, void *audio_buf, size_t audio_buf_len);
} game_api_t;
