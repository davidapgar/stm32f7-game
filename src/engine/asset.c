#include "asset.h"

#define ASSET_MAGIC (('G' << 0) | ('a' << 8) | ('m' << 16) | ('e' << 24))

typedef enum {
    ASSET_TYPE_NONE = 0,
    ASSET_TYPE_IMAGE,
    ASSET_TYPE_TILEMAP,
} asset_type_t;

typedef struct {
    uint32_t magic;
    uint32_t version;
    uint32_t type;
} asset_header_t;

typedef struct {
    uint32_t width, height;
    uint32_t tile_width, tile_height;
    uint32_t format;
} asset_image_header_t;

typedef struct {
    uint32_t width, height, tilesize, layercount;
} asset_tilemap_header_t;

static platform_handle_t asset_open(platform_api_t *platform, const char *name,
                                    asset_header_t *header, asset_type_t typ) {
    platform_handle_t f = platform->file_open(name, false);
    ASSERT_MSG(f, "Asset '%s' not found", name);
    platform->file_read(f, sizeof(asset_header_t), header);
    ASSERT_MSG(header->magic == ASSET_MAGIC, "Asset '%s' magic incorrect", name);
    ASSERT_MSG(header->type == typ, "Asset '%s' type incorrect", name);
    return f;
}

void asset_load_image(platform_api_t *platform, const char *name, bitmap_t *image) {
    asset_header_t header = {};
    asset_image_header_t imghdr = {};

    platform_handle_t f = asset_open(platform, name, &header, ASSET_TYPE_IMAGE);
    platform->file_read(f, sizeof(imghdr), &imghdr);
    image->width = imghdr.width;
    image->height = imghdr.height;
    image->tile_width = imghdr.tile_width;
    image->tile_height = imghdr.tile_height;
    image->format = imghdr.format;
    uint32_t bpp = bitmap_format_bytes_per_pixel(image->format);
    ASSERT_MSG(imghdr.width <= 256 && imghdr.height <= 256, "Image asset too large");
    ASSERT_MSG(imghdr.tile_width <= imghdr.width && imghdr.tile_height <= imghdr.height,
               "Image tile size too large");
    uint32_t bytesize = imghdr.width * imghdr.height * bpp;
    image->data = (uint8_t *)platform->alloc(bytesize);
    platform->file_read(f, bytesize, image->data);
    platform->file_close(f);
}

void asset_load_tilemap(platform_api_t *platform, const char *name, tilemap_t *map) {
    asset_header_t header = {};
    asset_tilemap_header_t maphdr = {};

    platform_handle_t f = asset_open(platform, name, &header, ASSET_TYPE_TILEMAP);
    platform->file_read(f, sizeof(maphdr), &maphdr);
    map->width = maphdr.width;
    map->height = maphdr.height;
    map->tilesize = maphdr.tilesize;
    map->layercount = maphdr.layercount;
    ASSERT_MSG(map->layercount < TILEMAP_MAX_LAYERS, "Map has too many layers");
    for (int i = 0; i < map->layercount; i++) {
        size_t bytesize = map->width * map->height;
        map->layers[i].data = platform->alloc(bytesize);
        platform->file_read(f, bytesize, map->layers[i].data);
    }
    platform->file_close(f);
}
