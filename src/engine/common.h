#pragma once

#include <stdio.h>
#include <stdlib.h>

__attribute__((__noreturn__)) void fatal_error(void);

#define ASSERT(expr)       \
    {                      \
        if (!(expr))       \
            fatal_error(); \
    }

#ifdef PLAT_DEBUG_PRINT

#define DEBUG_LOG(...) printf(__VA_ARGS__)

#define ASSERT_MSG(expr, ...)                          \
    if (!(expr)) {                                     \
        printf("ASSERT %s:%d - ", __FILE__, __LINE__); \
        printf(__VA_ARGS__);                           \
        printf("\n");                                  \
        fatal_error();                                 \
    }

#else

#define DEBUG_LOG(...)

#define ASSERT_MSG(expr, ...) ASSERT(expr)

#endif

#define ABS(v) ((v) > 0 ? (v) : -(v))
#define CLAMP(v, min, max) ((v) > (min) ? ((v) < (max) ? (v) : (max)) : (min))
