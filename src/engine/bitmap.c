#include <stdbool.h>
#include <string.h>

#include "bitmap.h"
#include "common.h"

// RGBA5551
#define RGBA5551_R(V) (((V) >> 11) & 31)
#define RGBA5551_G(V) (((V) >> 6) & 31)
#define RGBA5551_B(V) (((V) >> 1) & 31)
#define RGBA5551_A(V) ((V)&1)

// RGB565
#define RGB565_R(V) (((V) >> 11) & 31)
#define RGB565_G(V) (((V) >> 5) & 63)
#define RGB565_B(V) (((V) >> 0) & 31)

// clipping blit RGBA5551 -> RGB565
static void bitmap_blit_rgba5551(bitmap_t *dest, bitmap_t *src, int dx, int dy, int ssx, int ssy,
                                 int w, int h) {
    for (int j = 0; j < h; j++) {
        int sy = ssy + j;
        int py = dy + j;
        for (int i = 0; i < w; i++) {
            int sx = ssx + i;
            int px = dx + i;
            if (px >= 0 && py >= 0 && px < dest->width && py < dest->height) {
                uint16_t *sp = (uint16_t *)(src->data + (sy * src->width * 2) + (sx * 2));
                if (RGBA5551_A(*sp)) {
                    uint16_t *dp = (uint16_t *)(dest->data + (py * dest->width * 2) + (px * 2));
                    *dp = RGB565(RGBA5551_R(*sp), RGBA5551_G(*sp) << 1, RGBA5551_B(*sp));
                }
            }
        }
    }
}

// clipping blit RGB565 -> RGB565
static void bitmap_blit_rgb565(bitmap_t *dest, bitmap_t *src, int dx, int dy, int ssx, int ssy,
                               int w, int h) {
    for (int j = 0; j < h; j++) {
        int sy = ssy + j;
        int py = dy + j;
        for (int i = 0; i < w; i++) {
            int sx = ssx + i;
            int px = dx + i;
            if (px >= 0 && py >= 0 && px < dest->width && py < dest->height) {
                uint16_t *dp = (uint16_t *)(dest->data + (py * dest->width * 2) + (px * 2));
                uint16_t *sp = (uint16_t *)(src->data + (sy * src->width * 2) + (sx * 2));
                *dp = *sp;
            }
        }
    }
}

void bitmap_blit(bitmap_t *dest, bitmap_t *src, int dx, int dy, int sx, int sy, int w, int h) {
    // FIXME: currently assumes destination format of RGB565
    ASSERT(dest->format == BITMAP_FORMAT_RGB565);

    switch (src->format) {
        case BITMAP_FORMAT_RGB565:
            bitmap_blit_rgb565(dest, src, dx, dy, sx, sy, w, h);
            break;

        case BITMAP_FORMAT_RGBA5551:
            bitmap_blit_rgba5551(dest, src, dx, dy, sx, sy, w, h);
            break;

        default:
            ASSERT(false);
    }
}

void bitmap_blit_tile(bitmap_t *dest, bitmap_t *src, int dx, int dy, int tile) {
    const int tiles_per_row = src->width / src->tile_width;
    int sx = (tile % tiles_per_row) * src->tile_width;
    int sy = (tile / tiles_per_row) * src->tile_height;
    bitmap_blit(dest, src, dx, dy, sx, sy, src->tile_width, src->tile_height);
}

void bitmap_blit_full(bitmap_t *dest, bitmap_t *src, int dx, int dy) {
    // FIXME: currently assumes destination format of RGB565
    ASSERT(dest->format == BITMAP_FORMAT_RGB565);

    switch (src->format) {
        case BITMAP_FORMAT_RGB565:
            bitmap_blit_rgb565(dest, src, dx, dy, 0, 0, src->width, src->height);
            break;

        case BITMAP_FORMAT_RGBA5551:
            bitmap_blit_rgba5551(dest, src, dx, dy, 0, 0, src->width, src->height);
            break;

        default:
            ASSERT(false);
    }
}

void bitmap_fill(bitmap_t *dest, uint32_t colour) {
    switch (bitmap_format_bytes_per_pixel(dest->format)) {
        case 1:
            memset(dest->data, dest->width * dest->height, (uint8_t)colour);
            break;
        case 2:
            for (int i = 0; i < dest->width * dest->height; i++) {
                ((uint16_t *)dest->data)[i] = (uint16_t)colour;
            }
            break;
        case 4:
            for (int i = 0; i < dest->width * dest->height; i++) {
                ((uint32_t *)dest->data)[i] = colour;
            }
            break;
    }
}

uint32_t bitmap_format_bytes_per_pixel(bitmap_format_t format) {
    switch (format) {
        case BITMAP_FORMAT_RGB565:
        case BITMAP_FORMAT_RGBA5551:
            return 2;
        case BITMAP_FORMAT_RGBA8888:
            return 4;
        default:
            ASSERT(false);
    }
}
