#pragma once

#include "bitmap.h"
#include "common.h"
#include "platform.h"
#include "tilemap.h"

void asset_load_image(platform_api_t *platform, const char *name, bitmap_t *image);
void asset_load_tilemap(platform_api_t *platform, const char *name, tilemap_t *map);
