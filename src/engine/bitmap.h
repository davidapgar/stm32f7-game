#pragma once

#include <stdint.h>

// Pixel constructors
// Note these assume component values pre-scaled to the appropriate bitfield size.
// eg. for RGB565, max red is 31 (5 bits), while max green is 63 (6 bits).
#define RGBA5551(R, G, B, A) ((R) << 11) | ((G) << 6) | ((B) << 1) | (A)
#define RGB565(R, G, B) ((R) << 11) | ((G) << 5) | (B)

// Bitmap pixel formats
typedef enum {
    BITMAP_FORMAT_RGBA8888 = 1,
    BITMAP_FORMAT_RGB565 = 2,
    BITMAP_FORMAT_RGBA5551 = 3,
} bitmap_format_t;

typedef struct {
    uint16_t width, height;
    uint16_t tile_width, tile_height;
    bitmap_format_t format;
    uint8_t *data;
} bitmap_t;

// Copy all pixels from bitmap `src` into bitmap `dest` at position (dx,dy)
// Performs pixel format conversion and clipping as necessary.
void bitmap_blit_full(bitmap_t *dest, bitmap_t *src, int dx, int dy);

// Copy a tile from tilesheet bitmap `src` into bitmap `dest` at position (dx,dy)
// `tile` refers to tile index in the tilesheet, with tile size specified in the
// bitmap (`tile_width` / `tile_height` fields).
// Performs pixel format conversion and clipping as necessary.
void bitmap_blit_tile(bitmap_t *dest, bitmap_t *src, int dx, int dy, int tile);

// Copy (w x h) pixels from bitmap `src` at position (sx,sy) into bitmap `dest` at position (dx,dy)
// Performs pixel format conversion and clipping as necessary.
void bitmap_blit(bitmap_t *dest, bitmap_t *src, int dx, int dy, int sx, int sy, int w, int h);

// Fill a bitmap with a pixel of the given colour
// Note pixel must be packed in the correct format for the given bitmap.
void bitmap_fill(bitmap_t *dest, uint32_t colour);

// Return bytes per pixel for a given format
uint32_t bitmap_format_bytes_per_pixel(bitmap_format_t format);
