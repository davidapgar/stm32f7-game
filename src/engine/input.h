#pragma once

#include <stdbool.h>

typedef struct {
    bool up, down, left, right, a, b, c, d, menu;
    float dt;
} input_state_t;
_Static_assert(sizeof(input_state_t) == 16, "Unexpected input state size");
