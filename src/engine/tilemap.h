#pragma once

#include <stdint.h>

#include <bitmap.h>

enum {
    TILEMAP_MAX_LAYERS = 4,
};

typedef struct {
    uint8_t *data;
} tilemap_layer_t;

typedef struct {
    uint32_t width, height, tilesize, layercount;
    tilemap_layer_t layers[TILEMAP_MAX_LAYERS];
    bitmap_t tileset;
} tilemap_t;

// Draw layer of tilemap `map` to the bitmap `buffer`. Top left point of buffer
// starts at tilemap tile (px,py) (in pixel coordinates, not tiles).
void tilemap_render(tilemap_t *map, bitmap_t *buffer, int layer, int px, int py);
