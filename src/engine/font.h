#pragma once

#include "bitmap.h"

// Return builtin system font.
bitmap_t *font_system(void);

// Draw `text to bitmap `fb`, starting at pixel (x,y) using provided font.
// `font` must be a tileset bitmap where the tile size matches font character size.
void font_print(bitmap_t *fb, bitmap_t *font, int x, int y, const char *text);
