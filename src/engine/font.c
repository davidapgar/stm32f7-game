#include "font.h"
#include "sysfont.h"

// FIXME: hard-coded values from the asset
static const bitmap_t _font_system = {
    .width = 128,
    .height = 18,
    .tile_width = 4,
    .tile_height = 6,
    .format = BITMAP_FORMAT_RGBA5551,
    .data = (uint8_t *)(bin2c_fontsystem_4x6_dat + 32),
};

bitmap_t *font_system(void) {
    return (bitmap_t *)&_font_system;
}

void font_print(bitmap_t *fb, bitmap_t *font, int x, int y, const char *text) {
    while (*text) {
        int char_idx = (*text++) - ' ';
        bitmap_blit_tile(fb, font, x, y, char_idx);
        x += font->tile_width;
    }
}
