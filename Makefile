# vim: set ft=make:

MAKEFLAGS += --jobs=2
PLAT ?= sdl

SRCS := \
    example/game.c

GAME_ASSETS := \
    example/data/ball.dat \
    example/data/paddle.dat \
    example/data/testmap.dat \
    example/data/tiles_testmap_8x8.dat \

include src/plat_$(PLAT)/defs.mk
include common.mk
