#!/usr/bin/env python3

import os
import re
import sys
import struct
import json
import enum

from PIL import Image


class BitmapFormat(enum.IntEnum):
    """ This must match bitmap_format_t in bitmap.h """
    RGBA8888 = 1
    RGB565   = 2
    RGBA5551 = 3


class AssetType(enum.IntEnum):
    """ This must match asset_type_t in asset.c """
    IMAGE = 1
    TILEMAP = 2


def rgba8888_to_rgb565(r, g, b, a):
    return ((r >> 3) << 11) | ((g >> 2) << 5) | (b >> 3)


def rgba8888_to_rgba5551(r, g, b, a):
    a = 1 if a > 0 else 0
    return ((r >> 3) << 11) | ((g >> 3) << 6) | ((b >> 3) << 1) | (a)


def make_header(typ, version=1):
    return struct.pack("ccccII", b'G', b'a', b'm', b'e', version, typ)


def make_header_img(w, h, tilew, tileh, fmt):
    return make_header(AssetType.IMAGE) + struct.pack("IIIII", w, h, tilew, tileh, fmt)


def make_header_map(w, h, tilesize, layercount):
    return make_header(AssetType.TILEMAP) + struct.pack("IIII", w, h, tilesize, layercount)


def convert_img(w, h, tilew, tileh, data):
    rdata = bytes()
    for j in range(h):
        for i in range(w):
            pix = data[i,j]
            rdata += struct.pack("H", rgba8888_to_rgba5551(*pix))
    return make_header_img(w, h, tilew, tileh, BitmapFormat.RGBA5551) + rdata


def convert_map(mapjson):
    layercount = 0
    layers = b''
    w = h = None
    tilesize = mapjson['tilewidth']
    assert mapjson['tileheight'] == tilesize, 'Expected square tiles'
    for layer in mapjson['layers']:
        if layer['type'] == 'tilelayer':
            w, h = layer['width'], layer['height']
            assert max(layer['data']) <= 255, 'Tile index > 255'
            layers += bytearray(layer['data'])
            layercount += 1
        else:
            raise Exception('unhandled map layer %s' % layer['type'])
    return make_header_map(w, h, tilesize, layercount) + layers


def convert(args):
    infn, outfn = args
    in_name, in_ext = os.path.splitext(infn)
    in_file = os.path.basename(infn)

    if in_ext == '.png':
        img = Image.open(infn)
        img = img.convert('RGBA')
        w, h = img.size
        tilew, tileh = w, h

        if in_file.startswith('font') or in_file.startswith('tiles_'):
            fontmatch = re.match('.*_(\d+)x(\d+)', in_name)
            if not fontmatch:
                raise Exception('sprite sheets must in format \'filename_WxH.png\'')
            tilew, tileh = map(int, fontmatch.groups())
        data = convert_img(w, h, tilew, tileh, img.load())
    elif in_ext == '.json':
        mapjson = json.load(open(infn))
        data = convert_map(mapjson)
    else:
        raise Exception('unhandled file type %s' % in_ext)
    open(outfn, 'wb').write(data)


if __name__ == '__main__':
    convert(sys.argv[1:])
