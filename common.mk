# vim: set ft=make:

BINDIR ?= build/$(PLAT)
GAMEBIN := $(BINDIR)/game$(BINEXT)

CFLAGS := $(PLATFLAGS) $(INCS) -O$(OPT) -g -Wall -Werror -Wdouble-promotion -MMD -MP
CFLAGS += -std=gnu11

SRCS += \
    src/engine/bitmap.c \
    src/engine/asset.c \
    src/engine/tilemap.c \
    src/engine/font.c \
    src/engine/common.c \

OBJS = $(SRCS:%.c=$(BINDIR)/%.o) $(ASRCS:%.s=$(BINDIR)/%.o)
ASSETS = $(GAME_ASSETS:%.dat=$(BINDIR)/%.dat)
DEPS = $(OBJS:%.o=%.d)

all: $(GAMEBIN) $(PLATTARGETS:%=$(BINDIR)/%) $(ASSETS)

-include $(DEPS)

# code

$(GAMEBIN): $(OBJS)
	@echo "[LD]    $@"
	@$(CC) -o $@ $^ $(LDFLAGS)

%.bin: %.elf
	@echo "[OBJCP] $@"
	@$(OBJCOPY) -Obinary $< $@

%.lst: %.elf
	@echo "[LST]   $@"
	@$(OBJDUMP) -d $< > $@

$(BINDIR)/%.o: %.s
	@echo "[AS]    $@"
	@mkdir -p $(dir $@)
	@$(AS) $(ARCHFLAGS) -o $@ -c $<

$(BINDIR)/%.o: %.c
	@echo "[CC]    $@"
	@mkdir -p $(dir $@)
	@$(CC) $(CFLAGS) -c -o $@ $<

# assets

$(BINDIR)/%.dat: %.png
	@echo "[ASSET] $@"
	@mkdir -p $(dir $@)
	@./tool/assetconvert.py $< $@

$(BINDIR)/%.dat: %.json
	@echo "[ASSET] $@"
	@mkdir -p $(dir $@)
	@./tool/assetconvert.py $< $@

systemfont: $(BINDIR)/example/data/fontsystem_4x6.dat

# misc

run: $(GAMEBIN) $(ASSETS)
	ASAN_OPTIONS=detect_leaks=0 ./$<

debug: $(GAMEBIN) $(ASSETS)
	$(GDB) $(DEBUGARGS) $<

debugserver:
	openocd -f misc/target.cfg

.PHONY: clean
clean:
	@echo "[CLEAN]"
	@rm -rf $(BINDIR) $(OBJS) $(DEPS) $(ASSETS)

format:
	find . -name "*.c" -o -name "*.h" | grep -v "lib" | xargs clang-format -i

wc:
	find . -name "*.c" -o -name "*.h" | grep -v "lib" | xargs wc -l | sort -n
