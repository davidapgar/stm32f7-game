# Project Status

This page attempts to give a high-level technical overview of the current state of the project. For a historical log of design decisions (though they may not entirely reflect the current design of the code/hardware) see the [RFCs][rfcs].

# Software Architecture

A game built with the game engine can be divided into 3 major components:

1. Platform layer. The platform layers is platform specific (PC or STM32) and abstracts away low-level hardware from the game engine. The primary interface to higher level code is the `game_api_t` structure which must be implemented by game code. The platform layer runs the main loop and calls a `game_tick` function every frame, passing in user input as well as a framebuffer for the game to render to.
2. Engine. The game engine is a collection of loosely coupled libraries that provide higher level abstractions on top of the platform `game_api_t` layer. For example the engine provides simple libraries to load images from disk and quickly draw them to the screen. The engine is platform agnostic and depends only on the platform API to interact with hardware.
3. Game logic. Platform agnostic game-specific logic is implemented on top of the engine. This logic is provided by developers (not part of this repo, other than the example game).

```
.--------------------.
|    Game Logic      |
|     example/       |
'--------------------'
 engine/*.h interface
.--------------------.
|       Engine       |
|     src/engine     |
.--------------------'
 game_api_t interface
.--------------------.
|   Platform Layer   |
| src/plat_[sdl|stm] |
'--------------------'
```

## Current Status

* Platform Layer
    * Mac / Linux platform support using SDL
    * STM32 platform support on [STM32F746-Discovery][stm32f7disco].
    * Platform API includes:
        * Memory allocation
        * File I/O (from SD card on STM32)

* Engine
    * Basic asset system for loading data from disk
    * Support for blitting RGB565 and RGBA5551 images to the framebuffer

* Tooling
    * Asset converter tool to convert images to native asset format

# Hardware Architecture

TODO. Custom hardware currently unimplemented. Development currently on [STM32F746-Discovery][stm32f7disco] board.

[rfcs]: https://bitbucket.org/rng/stm32f7-game/src/master/doc/
[stm32f7disco]: https://www.st.com/en/evaluation-tools/32f746gdiscovery.html
