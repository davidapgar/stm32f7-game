# Summary

Add support for [tile-based maps][tilemap] rendering in the engine and asset
pipeline.

# Background

Most consoles from the GBA-era (and before) include hardware support for
tile-based rendering. One of the core drivers behind tile-based maps is that
they allow large, complex worlds with relatively low memory usage. As an
example, imagine a 320x240 pixel world (4 of our 160x120 pixel LCD screens):

    map size: 320 * 240 * 2         = 150KB (320x240 pixels at 16 bits/pixel)

Now assume we took that same world and broke it down into 8x8 pixel tiles.
Let's also assume we have a tileset with 256 tiles, such that each tile
reference can fit in 1 byte:

    tile size: 8 * 8 * 2            =   128B (8x8 pixels at 16 bits/pixel)
    tileset size: 128B * 256        =   32KB (128B * 256)
    map size: (320 / 8) * (240 / 8) =  1.2KB
    map + tileset size              = 33.2KB

The resulting tile map is less than a 25% of a full bitmap version of the map.
Since our platform is resource constrained, and given there is great support
(tool-wise) for tile maps, we'll implement a basic tile map asset and rendering
system.

# Overview

Tilemaps share a lot of the code we built for fonts in [RFC-0007][rfc7]. Rather
than the characters in a string indexing into the font sprite sheet, our
tilemap indexes into a tilesheet. To create a tilemap there's thankfully a
great open-source tool in [Tiled][tiled]. Tiled allows exporting into a simple
JSON format which can then be converted into a raw binary asset by our asset
converter. Rendering a tilemap in this format is a matter of blitting tiles
from a tileset image to the framebuffer using our sub-image blitter implemented
for fonts.

# Implementation

We'll start with our tilemap (in-memory) structure:

    typedef struct {
        uint32_t width, height, layercount;
        uint8_t* layers[TILEMAP_MAX_LAYERS];
        bitmap_t tileset;
    } tilemap_t;

Our maps are made up of two main components:

1. An array of layers, each of which is an array of (byte) tile indices.
2. A tileset in the form of a bitmap

Multiple layers are easy to implement, and supported by Tiled. They enable
things like foreground/background layers and [parallax][parallax]. Our tileset
image will use the tile width/height parameters we implemented for fonts to
define tileset sizes.

## Asset format and conversion

Our tilemap asset header will be similar to a bitmap:

    typedef struct {
        uint32_t width, height, layercount;
    } asset_tilemap_header_t;

The header will be followed by `layercount` number of layers, each of which is
`width` * `height` bytes of tile indices. Conversion from a Tiled map to our
asset format is made much simpler by a well documented Tiled [JSON
format][jsonformat]. Conversion is a simple matter of writing out tile indices,
which are easily extracted from the JSON:

    layers = b''
    layercount = len(mapjson['layers'])
    for layer in mapjson['layers']:
        layers += bytearray(layer['data'])

Tiled supports a number of additional map features which we'll ignore (for
now).

## Tilemap rendering

Tilemap rendering needs to take a few requirements into account:

- Empty tiles
- Transparent tiles
- Pixel offset for smooth map scrolling

Empty and transparent tiles are the simplest to handle. For a location on the
map where no tile was placed Tiled saves a 0 as the tile index, while placed
tiles are indexed starting at 1. So when rendering our tiles we simply skip any
tile index of 0, and subtract 1 from any other index when looking up in our
tileset.  For partially transparent tiles (eg grass), we can simply store tiles
in RGBA5551 format and render them with our transparency-aware blitter (as with
fonts).

Pixel offset rendering is more complex (but not much more so) - it's just a
little math (outlined for the X axis below):

    pos_x_px = ...   // pixel position into the map to start rendering from
    view_w_px = ...  // width of the viewport
    view_w_tiles = view_w_px / tile_w

    // calculate both starting tile inside our viewport, and the offset into
    // that tile in pixels.
    tile_x_start = x_pos_px / tile_w
    ofs_x = x_pos_px % tile_w
    tile_x_px = -ofs_x

    // render a row of tiles
    for tile_x in range(tile_x_start, tile_x_start + view_w_tiles):
        tile_idx = map[tile_x][tile_y]
        if tile_idx:
            blit_tile(tileset, tile_idx - 1, tile_x_px, tile_y_px)
        tile_x_px += tile_w

Our tile blitting needs to take clipping into account as this code draws the
first column/row of tiles at a negative offset, but otherwise is
straightforward.

# Concerns / Open Questions

Tiled supports a range of additional features on top of simple tile maps. It'll
be worth exploring whether make our converter and asset formats aware of the
following features:

- Object editor (for placing in game entities)
- Tile animator (for creating animated backgroudns)

When we start rendering multiple layers, we'll need to investigate performance,
given there may be significant overdraw (top layers hiding pixels drawn by
lower layers, leading to wasted tile blitting work). Additionally tilemap
layers include a lot of empty space, and we may need to explore some form of
compressed in-memory representation for the maps.

[tilemap]: https://en.wikipedia.org/wiki/Tile-based_video_game
[tiled]: https://www.mapeditor.org/
[rfc7]: https://bitbucket.org/rng/stm32f7-game/src/825f5a2e445102842324ec3df32c7a9086fd1e55/doc/rfc-0007-fonts.md
[parallax]: https://en.wikipedia.org/wiki/Parallax
[jsonformat]: https://doc.mapeditor.org/en/stable/reference/json-map-format/
