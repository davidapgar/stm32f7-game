# Summary

An outline for the CPU & memory choices for the game console and implications
on software.

# Background

This RFC dives into the microcontroller choices for the game console. It tries
to reference external sources for context/further reading, but assumes a basic
understanding of [Microcontroller][micros] and embedded hardware.

# Overview

The console will be based on an [STM32][stm32] [Cortex-M7][cortex-m7]
microcontroller using only on-chip RAM and [Flash][flash]. Games and data will
be loadable from SD. Display will be 16 bit/pixel at ~160x120 resolution.

Microcontroller choice has implications on the final console in a number of
ways:

- CPU performance determines complexity of games the console is capable of
  running.
- RAM size (and on-chip vs off-chip memory capabilities) also determine game
  asset complexity.
- On-chip vs off-chip program memory impacts user-loadable game structure

We want to drive the choice based on a number of goals and constraints:

- Simple hand-assembly of board. No [BGA][bga] components and preferably no
  [QFN][qfn].
- Simple PCB layout. Prefer fewer high speed [buses][bus] where possible.
- System powerful enough to do [GBA][gba]/[SNES][snes] class games.
- Good enough standard [dev board][devboard] for those that don't want to build
  a custom board.

# Implementation

Some initial assumptions:

- The microcontroller will be an [ARM Cortex M class][cortex-m] chip. While
  other options are available (MIPS, PIC32's, RISC-V, etc), the combination of
great open source tooling, documentation, dev kits, and wide range of chips
with powerful peripherals, makes the ARMs current best option.

- The display will be colour. A monochrome display would reduce memory
  requirements somewhat, but there's a wider range of cheap colour displays and
it gives a much more interesting game dev scope. Display resolution/colour
depth has a significant impact on memory as [framebuffers][framebuffer] will be
large (relative to total RAM). A 32bit (RGBA) 320x240 framebuffer is 300KB.

The variables we have to work with:

- CPU speed
- On chip RAM size
- On chip Flash size
- External bus options for RAM / Flash (can we use external memories with the
  micro, and via what buses).
- Display resolution / depth.

Many of these variables are interconnected and so are not independently
adjustable (for example a given microcontroller range will have a limited set
of RAM size options).

### CPU

The class of ARM Cortex micro's we'll be considering are the [M4][cortex-m4]
and [M7's][cortex-m7]. These micros typically come in 100-400MHz core clock
range, and have hundreds of KB of RAM and Flash. They also include [SIMD
instructions][simd] (useful for audio and graphics optimisation), floating
point hardware, and a range of powerful peripherals (SD/MMC, USB Device/Host,
smart [DMA][dma], etc).

We start with the most restrictive requirement: dev board. While the eventual
goal is a custom board, we'd like to use an existing dev board that covers most
of the functionality for initial development (as well as for those who don't
want to build a custom board). [NXP][nxp] and [ST][st] both provide good dev
boards for with LCDs:

- NXP [OM13092][om13092]: LPC546xx Cortex-M4 @ ~200Mhz, $74
- ST [STM32F746G-DISCO][stm32f-disco]: STM32F74x Cortex-M7 @ ~200Mhz, $56

Both include a 4.3" 480x272 LCD, touchscreen, expansion flash and RAM, USB, SD,
audio out, and onboard [debug hardware][jtag].

In terms of part packages the LPC comes in [LQFP][lqfp] (100 & 208 pin) and
TFBGA (180 pin) packages. Of the LQFP packages only the 208 pin part has LCD
support. The STM comes in LQFP (100 pin), UFBGA (176 pin) and TFBGA (240 pin).
Interestingly the STMF7 parts are typically pin compatible with the
[STM32H7][stm32h] parts which provide higher clock rates and RAM sizes.

The LQFP-208 LPC comes in a couple variants but the most commonly available is
the [LPC54606][lpc546] with 512K Flash and 200K RAM at $8.60 in single unit
quantities.

The LQFP-100 STM comes in 512K/1M/2M Flash and 320K/512K RAM variants ranging
from $11.37 to $15.59 in single unit quantities. Low end [STM32F745][stm32f746]
has 512K Flash and 320K RAM.

In the end both chips are roughly equivalent and could easily support the needs
of the console. We're going to go with the STM32 however, for a couple reasons:

- Familiarity - I have worked with the STM32 chips and their tooling a bunch it
  has always been a pleasant experience.
- Smaller package - the LQFP-100 will be easier for beginners to solder than
  the -208
- Cheaper dev board - that much less of a hurdle to get started.
- STM32H upgrade path - very nice to have a (in theory) drop in replacement
  part that ups clock and RAM.

### Framebuffer sizing

Before diving into Flash and RAM options, lets take a brief detour through the
content we're considering as that will inform our decisions on memory sizing.
For reference, the GBA and SNES could display images with [15bit colour][15bit]
(32768 colours) at 240x160 or 256x224 respectively. Typically sprite /
background data would be stored at lower colour depth (using colour
[palettes][palette]), but these max values give us a good starting point for
upper limits. 256 x 224 x 2 (15 bit pixel packed into 2 bytes) is 112K.
[QVGA][qvga] (320x240, a common LCD resolution) at 16bit is 150K. This is 46%
of the 320K on-chip RAM in the low end STM32. If we [double
buffer][doublebuffer] and use the 512K STM32, that's still almost 60% of RAM
used just for frame buffers. As such we'll limit ourselves to 240x160 max which
results in a 75K framebuffer (38K if 8bit palette is used).

### RAM options

Returning to RAM - with the STM32F7 we have the choice between 320K and 512K
RAM, but both can support significantly more additional off-chip RAM. The
compromise here is that off-chip RAM requires both an additional chip and more
complex PCB (routing the dense, high speed interconnect between the CPU and
RAM). Given the framebuffer calculations above, it makes sense to go with the
512K variant without additional off-chip RAM. This gives the best compromise of
total RAM size while keeping board layout simple.

### Flash options

Flash is a more complex story. We have 4 choices for storing code and data:

- On-chip Flash
- Off-chip [QSPI][qspi] Flash
- Off-chip SD
- On-chip RAM (copied from somewhere else on boot)

Unlike external SDRAM, QSPI requires fewer traces (6) so is simpler to route on
the PCB. Both on-chip and Off-chip flash can have programs executed directly
from them ([XIP][xip]). Code may be stored on SD cards, but will need to be
copied elsewhere before it can be executed. One assumption we'll make is that
game data (sprites, levels, sound effects, music, etc) will be larger than game
code. Game data doesn't need to be stored in executable storage and can be
streamed into RAM as needed. This gives us more flexibility about where we will
store game code as it's smaller. We'd like to allow easy sharing of games which
requires some sort of external storage. The simplest option here would be to
store games on SD and copy their code to RAM on startup. This would be quick
but comes at the cost of our already scarce RAM. Alternatively we could copy
code from SD to Flash (on or off-chip), though flash writes are much slower
than RAM (10s of K/s for Flash vs M/s for RAM). Adding to the complexity is
that most embedded code is typically linked at a fixed address - this means the
code expects to be at a specific location in the processor's memory space
(while [PIC][pic] is feasible, it's quite a bit more involved and has
performance implications). So if we have multiple games in flash, they either
all need to linked at their own locations, or need to be reordered depending on
which game needs to be run (again incurring the flash write time cost).

Overall there's no easy option without switching to lots of off-chip RAM. We'll
still try to provide as much flexibility for developers as possible. We'll use
an SD card slot on the board with the following assumptions:

- Games can be stored and shared on SD.
- Game code will be < 100K, Game data will be hundreds of K.
- Game code will be linked to an address in RAM and loaded on boot.
- Game data will be streamed into RAM from SD as needed.

This comes at a definite RAM cost, but provides the most seamless game play +
sharing experience. 

Developers can choose to link and store games in Flash, which provides more RAM
space for content, at the cost of slower game install/load times.

# Alternatives

A couple alternatives considers:

- External SDRAM. Much larger total RAM availability means games (and their
  content) could be larger, though it comes at the cost of more complex PCB
layout (and potentially cost). This also increases the complexity of the
latency story around various parts of RAM (on-chip vs off). In the interest of
simplicity we've stayed with only on-chip RAM.

- Non-SD QSPI cartridges. We could make custom game cartridges using QSPI
  chips. This would allow execution of game code directly from the cartridge,
increasing game RAM availability. However this would mean building custom
cartridge PCBs / sockets.

# Concerns / Open Questions

-

[micros]: https://en.wikipedia.org/wiki/Microcontroller
[stm32]: https://www.st.com/en/microcontrollers/stm32-32-bit-arm-cortex-mcus.html
[cortex-m7]: https://en.wikipedia.org/wiki/ARM_Cortex-M#Cortex-M7
[flash]: https://en.wikipedia.org/wiki/Flash_memory
[bga]: https://en.wikipedia.org/wiki/Ball_grid_array
[qfn]: https://en.wikipedia.org/wiki/Quad_Flat_No-leads_package
[bus]: https://en.wikipedia.org/wiki/Bus_(computing)
[gba]: https://en.wikipedia.org/wiki/Game_Boy_Advance
[snes]: https://en.wikipedia.org/wiki/Super_Nintendo_Entertainment_System 
[devboard]: https://en.wikipedia.org/wiki/Microprocessor_development_board
[cortex-m]: https://en.wikipedia.org/wiki/ARM_Cortex-M
[framebuffer]: https://en.wikipedia.org/wiki/Framebuffer
[cortex-m4]: https://en.wikipedia.org/wiki/ARM_Cortex-M#Cortex-M4
[simd]: https://en.wikipedia.org/wiki/SIMD
[dma]: https://en.wikipedia.org/wiki/Direct_memory_access
[nxp]: https://www.nxp.com/
[st]: https://www.st.com/
[om13092]: https://www.digikey.com/product-detail/en/nxp-usa-inc/OM13092UL/568-13354-ND/6618785
[stm32f-disco]: https://www.digikey.com/product-detail/en/stmicroelectronics/STM32F746G-DISCO/497-15680-5-ND/5267791
[jtag]: https://en.wikipedia.org/wiki/JTAG#Debugging
[lqfp]: https://en.wikipedia.org/wiki/Quad_Flat_Package
[stm32h]: https://www.st.com/content/st_com/en/products/microcontrollers/stm32-32-bit-arm-cortex-mcus/stm32-high-performance-mcus/stm32h7-series.html?querycriteria=productId=SS1951
[lpc546]: https://www.nxp.com/docs/en/data-sheet/LPC546XX.pdf
[stm32f746]: https://www.st.com/content/ccc/resource/technical/document/datasheet/96/ed/61/9b/e0/6c/45/0b/DM00166116.pdf/files/DM00166116.pdf/jcr:content/translations/en.DM00166116.pdf
[15bit]: https://en.wikipedia.org/wiki/List_of_monochrome_and_RGB_palettes#15-bit_RGB
[palette]: https://en.wikipedia.org/wiki/Palette_(computing)
[qvga]: https://en.wikipedia.org/wiki/Graphics_display_resolution#320_%C3%97_240_(QVGA)
[doublebuffer]: https://en.wikipedia.org/wiki/Multiple_buffering
[qspi]: https://en.wikipedia.org/wiki/Serial_Peripheral_Interface#Quad_SPI
[xip]: https://en.wikipedia.org/wiki/Execute_in_place
[pic]: https://en.wikipedia.org/wiki/Position-independent_code
