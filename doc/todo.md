# Open Projects / TODOs

## Small

- [GFX] Add 32bit RGBA bitmap/blitting
- [GFX] Add 24bit RGB bitmap/blitting
- [GFX] Add 8bit greyscale bitmap/blitting
- [MSC] Add commandline configurable PC pixel scaling
- [MSC] Add submodule-aware makefile for games submoduling the engine

## Medium

- [SND] Add platform audio API
- [GFX] Add palette-setting platform API
- [GFX] Add palettised 8bit bitmap/blitting
- [GFX] Add mode setting (resolution/bit-depth) platform API
- [GFX] Add colour-key bitmap transparency
- [GFX] Add sprite animations
- [MAP] Add tilemap collision system
- [AST] Add asset metadata file parsing
- [DBG] Add debug print on STM & PC
- [DBG] Add basic profiler
- [DBG] Add fault handler and info screen on STM
- [DBG] Add backtrace handler on PC
- [HW] Choose LCD module

## Large

- [SND] Add sound/music synthesizer
- [ENT] Add core object/entity system
- [GFX] Optimise (SIMD) bitmap blitter
- [AST] Add asset hotloading support
- [MSC] Add USB mass storage to STM platform to load game data files
