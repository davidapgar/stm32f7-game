# Summary

Outlines the flow of image data from initial creation on a PC to use in the
game engine.

# Background

Our games will have a variety of assets; [sprites][sprite], [background
tiles][tileset], maps, sound effects, music, etc. All of these assets will be
authored/edited on a developer's PC before being used in the game. In many
cases the format used to edit/store the asset on the PC won't be the same
format we want our engine to consume. As an example, many of our sprites or
tilesets are likely to be stored in [PNG][png] format. PNG is supported by  all
image editing tools, but is relatively complex for our needs (chunks,
compression, filtering, etc - see the `file format` section in the Wikipedia
page). As such we will convert source PNG images into a custom format more
easily consumed by our engine. In the case of some assets (tile maps for
example), a single source asset may transform into multiple files consumed by
the engine.

This doc outlines the flow ("asset pipeline") for converting source assets into
files consumed by the engine (using the [File IO platform API][fileio]).

# Overview

We're going to start super-simple; Source assets will be converted into engine
assets using a simple python script glued together with [Make][make]. Any
dependencies between assets (eg "when level1.map changes, run `convert_map.py`
on level1.map to generate level1.tiles and level1.lights") can be expressed in
the Makefile.

All of the asset conversion process will be run on the developer machine as
part of building the game.

    <--------- on development machine ---------> <- on console ->

    .-----------.     +------+    +-----------+    .-----------. 
    | +----------+    |      |    |   Asset   |    | +----------+
    | |  Source  |--->| Make |--->| Converter |----->|  Engine  |
    '-|  assets  |    |      |    |           |    '-|  assets  |
      +----------+    +------+    +-----------+      +----------+

Engine assets will be optimised for easy loading in the game. As such they
will initially be very simple - just raw dumps of the pixel/sound data with a
small metadata header.

# Implementation

This RFC will focus on image assets, but most of the ideas / code will apply to
other game assets.

## Engine Asset Format

Lets start by defining the file format we'll use for our engine assets. A
couple of assumptions / goals up front:

- Prefer simple loading code over optimised space on disk / in memory
- Assume we will iterate on the file format
- We don't care about backward compatibility (if we change the file format, we
  can just regenerate all assets).

With that in mind, lets look at the file format (in C syntax):

    typedef struct {
        uint32_t magic;   // 4 byte magic number
        uint32_t version; // version of the struct
        uint32_t type;    // type of asset
        union {           // one of the following asset-specific structures
            struct {
                uint32_t width, height, format;
                uint8_t data[];
            } img;
            struct {
                ...
            } map;
            ...
        };
    } asset_header_v1_t;

Each asset starts with a 12 byte (3 word) header. The header starts with a 4
byte [magic number][magic] which acts as a simple sanity check that the file
we're loading is an asset. This is followed by a 4 yte version number which
allows us to modify the file format over time (that is the asset structure
defined above is version 1, but as long as the first two words stay the same in
newer versions, any loader can read the version, then use that to determine
what the following bytes in the file mean). Finally a 4 byte enum which
indicates the type of asset (image, sound, map, etc), and thus structure of the
remaining data.

For an image we will store the width and height (in pixels), as well as the
format of the pixel data. The format defines how pixel data is packed into
bytes, and initially we will only support [RGB565][rgb565] (2 bytes per pixel)
as it is the native format of the LCD on the STM32 dev board. This means an 8 *
16 sprite will consume 280 bytes on disk / in memory:

    asset header:  12 bytes (3 x 4b words)
    image header:  12 bytes (3 x 4b words)
    image data:   256 bytes (8 * 16 * 2b pixels)
    -----------------------
    total         280 bytes

This of course adds up as we get to larger images (eg a full title screen is
37K, 11% of STM32 RAM), but the asset format allows for more compact encoding
down the line (eg palletised images using 4-8 bits per pixel).

## Conversion

Given the asset format we now need our asset converter to read source images
and emit files in our engine asset format. For images we can take advantage of
python image handling libraries such as [Pillow][pillow] to load source images.
This makes our image converter remarkably simple:

1. load PNG source image using Pillow (giving an array of pixel values)
2. Convert Pillow pixel array into RGB565 pixel data
3. Write out header + image data

For now we'll assume that we can determine asset type based on the input file
extension (eg .png=image, .wav=sound, etc) and so the converter can take in any
asset, switch based on extension, and process the asset accordingly. A simple
Makefile rule can then be used to convert any asset our game depends on:

    %.asset: %.png
    	./tool/assetconvert.py $< $@
    
# Concerns / Open Questions

- Currently the image format doesn't handle more "interesting" image data like
  animated sprites, or transparency. Both can likely be handled with small
  additions to the image asset structure.

[sprite]: https://en.wikipedia.org/wiki/Sprite_(computer_graphics)
[tileset]: https://en.wikipedia.org/wiki/Tile-based_video_game
[png]: https://en.wikipedia.org/wiki/Portable_Network_Graphics
[fileio]: https://bitbucket.org/rng/stm32f7-game/src/c460bca3b52db41b479b189aad007cd3ac145472/doc/rfc-0004-platform-api.md
[make]: https://www.gnu.org/software/make/
[magic]: https://en.wikipedia.org/wiki/Magic_number_(programming)#Format_indicator
[rgb565]: https://github.com/marco-calautti/Rainbow/wiki/RGB565
[pillow]: https://pillow.readthedocs.io/en/stable/handbook/overview.html
