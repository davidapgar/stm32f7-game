# Summary

Outline the asset system changes to support sprite sheets and apply them to
drawing fonts.

# Background

Currently our [asset system][rfc5] supports images in a RGB565 and RGBA5551
pixel formats. However, in many situations in-game objects will multiple
related images. For example an animation is a sequence of related images played
back in quick succession. [Tile maps][tilemap] also require a group of images
assembled together to form a game level. In both cases we could use our asset
system to load individual images for each animation frame or tile. But doing so
is wasteful as often each frame/tile is the same size for a given
animation/tile set. It'd be useful to have a way to store the whole animation
or tile set in a single asset. This is where [sprite sheets][spritesheet] (also
known as a 'texture atlas') come in: we can pack multiple frames / tiles into a
single image asset, with some metadata indicating where to find individual
frames.

# Overview

We'll implement sprite sheets with uniform-sized sub-images by adding a
`sprite_width` and `sprite_height` fields in our image asset header. Normal
(non sprite sheet) images will just set these fields to the size of the full
image.

By restricting ourselves to simple [monospace][monospace] fonts, we can easily
implement font rendering using a sprite sheet of characters. If the sprite
sheet characters are [ASCII][ascii]-ordered, sprite index is simply the ASCII
value of each character in a string.

# Implementation

We'll start by extending our image asset header to be as follows:

    struct {
        uint32_t width, height;               // size of overall image
        uint32_t sprite_width, sprite_height; // size of each sub-image
        uint32_t format;                      // pixel format
        uint8_t data[];                       // pixel data
    } asset_header_image_t;

For normal images, the `sprite` fields can be ignored. In sprite sheet images,
we'll arrange our sprites in [row-major order][rowmajor]. This means if we have
a 16x16 image containing 4 8x8 sprites it'll look like this:

       0       8      16
     0 +-------+-------+
       |       |       |
       |   0   |   1   |
       |       |       |
     8 +-------+-------+
       |       |       |
       |   2   |   3   |
       |       |       |
    16 +-------+-------+

To calculate row and column positions for a given sprite number we can do the
follow:

    num_sprites_per_row = image.width / image.sprite_width
    row = sprite_index % num_sprites_per_row
    col = sprite_index / num_sprites_per_row

Which gives us (0-based) sprite row and column indices for a given sprite. With
those indices we can multiply by sprite width and height values to get pixel
positions of a given sprite. Tile-based blitting can then be added to our
bitmap blitting library by blitting only a subset of an image's pixel data.

## Asset converter

Not much has to change in our asset converter. Our source images are still
PNGs, we just need to feed it additional metadata on sprite sizes. While longer
term a asset metadata file make sense, for now we'll just encode this
information in the filename. So our converter will look for `<width>x<height>`
info in the input image filename. For example `spritesheet_8x16.png` will be
output as an image asset with `sprite_width=8` and `sprite_height=16`.

## Fonts

We'll restrict ourselves to monochrome, monospace fonts. This lets us use a 96
element sprite sheet (120x28 pixels for 96 5x7 characters) to represent all the
printable [ASCII][ascii] characters (space to tilde).

Font rendering then can be implemented as (in pseudo-code):

    x = 0
    for char in string:
        sprite_index = char - 32 # (start at space character)
        bitmap_blit_sprite(framebuffer, font, x * font.sprite_width, y, sprite_index)
        x += font.sprite_width

# Alternatives

We could use a standard font format such as [TTF][ttf] or [BDF][bdf], but given
the low-resolution nature of our platform, these end up being wastefully
complex. Additionally, there's already a number of great 
[font sprite sheets][fontsheets] which we can use.

# Concerns / Open Questions

Proportional fonts (and more generally non-uniform sprite sheets) are useful
and our current system doesn't support them. A more complex metadata system
will be needed in our assets to support this case. This could be added later as
a new version of the image asset, or as a new asset type.


[rfc5]: https://bitbucket.org/rng/stm32f7-game/src/master/doc/rfc-0005-asset-pipeline.md
[tilemap]: https://en.wikipedia.org/wiki/Tile-based_video_game
[spritesheet]: https://en.wikipedia.org/wiki/Texture_atlas
[monospace]: https://en.wikipedia.org/wiki/Monospaced_font
[ascii]: https://en.wikipedia.org/wiki/ASCII
[rowmajor]: https://en.wikipedia.org/wiki/Row-_and_column-major_order
[bdf]: https://en.wikipedia.org/wiki/Glyph_Bitmap_Distribution_Format
[ttf]: https://en.wikipedia.org/wiki/TrueType
[fontsheets]: https://opengameart.org/content/fonts-0
