# Summary

This project aims to build a 2D game engine for a small microcontroller as well
as an associated piece of ([Gameboy][gba]-like) hardware on which to run games.
The primary goal is to serve as an educational piece of software & hardware and
so documentation of the design and development process is vital.  This doc is
intended to set the frame for future design / dev docs covering development of
the engine and hardware.

# Overview

A range of changes to the hardware and software in this repo will be simple
enough to be explained in a single commit message. For example fixing a small
bug in firmware or adding a clearer silkscreen to the PCB layout. These changes
can be a landed with a single Pull Request (PR). Other changes, such as new
software or hardware subsystems are more complex and will require some upfront
design. These changes should be preceded by a Request For Comment doc (RFC)
before a PR is opened with the implementation.

The RFCs serve a few of purposes:

- Explain the motivation, compromises, and alternatives for a given change. 
- Teach the reader about the system.
- Serve as a place for feedback on the design. Other contributors may have
  experience in the specific feature and can provide advice / improvements /
alternatives.

Requiring RFCs for significant changes will likely slow down the rate of
contribution to the project, but my hope is it will encourage high quality,
well documented contributions, as well as serve as educational material for
future readers/contributors.

# Process

The RFC process is intended to be lightweight and quick:

- Copy the [template](rfc-0000-template.md) to a new RFC in `doc/`.
- Fill in the RFC details.
- Open a pull request
- Get feedback, discuss, integrate changes.
- Get approval from at least one committer, then merge the RFC PR.
- Implement and PR (with a link to the RFC) the change described by the RFC.

[gba]: https://en.wikipedia.org/wiki/Game_Boy_Advance
