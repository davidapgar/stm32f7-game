# Summary

Design the high-level game engine architecture, specifically the interface
between platform-agnostic game code and platform-specific hosting code. Discuss
plans for Mac/PC + STM32 host code.

# Background

This RFC focuses primarily on the main game loop as the point to separate
platform and game code. The [Game Programming Patterns][gameloop] article is
great background reading.

# Overview

One primary goal is for our game engine to be runnable on both Mac/PC and the
final STM32 embedded target. This means we will have to abstract away the
platform specific portions of the code (reading input, driving audio/display
hardware, etc) from the platform agnostic bits (the actual game code).

STM32 is our most restrictive platform and sets the limits for what design our
engine around. This means, compared to a modern game we will have quite limited
features - no 3d hardware, minimal [chiptune][chiptune]-esque audio, single
threaded, etc. Since all of those features would require complex interfaces
between game and platform code (eg the [OpenGL][opengl] API), we can use this
to our advantage.

# Implementation

Lets start with a simple example game main function:

    void main() {
        hardware_init();
        gamestate state = game_init(state);

        while (1) {
            inputstate input = read_input();
            process_input(state, input);
            render_graphics(state);
            play_audio(state);
            wait_for_next_frame();
        }
    }

In this code we first initialise the platform's hardware, then we enter an
infinite loop. The loop reads user input, which is then fed to game code that
processes the input to update game state (move the player character for
example). The state is then used to render graphics and play audio (eg draw the
player character sprite and play the jump sound). Once output is performed, we
wait until the next frame (this assumes we're locking to the refresh rate of
the display, for example 60hz, so 1 frame every 16.6ms).

This code doesn't have a clear distinction between platform and game code, so
lets talk about the data that would need to be transferred between the two
subsystems (inferred from the above code).

1. Platform provides the game code with input state (which keys are pressed)
2. Game code provides the platform with an image to display each frame
3. Game code provides the platform with the chunk of audio to display each
   frame
4. Platform provides the game code with parameters for the above (display
   resolution, audio rate and depth, etc).

## Platform separation

Using these let's rewrite the game main loop:
    
    void main() {
        platformparams params = hardware_init();
        gamestate state = game_init(params);

        while (1) {
            imagebuffer display;
            audiobuffer audio;
            inputstate input = read_input();
            float dt = time_since_last_frame();
            game_update(dt, state, input, display, audio);
            hardware_copy_to_display(imagebuffer);
            hardware_start_audio(audio);
            wait_for_next_frame();
        }
    }

While this makes the code a little more complex at first glance, it also makes
an important separation: we now have game code that only interacts with generic
input and image/audio buffers. The platform code feeds the game code input
state then takes the image/audio buffers and drives the platform hardware to
display/play the buffers. The interface between our game and platform code is
now:

    gamestate& game_init(platformparams &params);
    void game_update(float dt, gamestate &state, inputstate &input,
                     imagebuffer &display, audiobuffer &audio);

The respective structures would look something like:

    struct platformparams {
        int display_width, display_height;
        int audio_rate_hz;
        // etc
    }

    struct inputstate {
        int up_pressed, down_pressed, ... ;
    }

    struct imagebuffer {
        uint8_t pixels[display_width * display_height * bit_depth];
    }

    struct audiobuffer {
        int16_t frames[audio_rate_hz * frame_time];
    }

The above functions and types can now define a header the platform exposes to
the game code. Any C file that implements those two functions can be linked
against any platform code (PC or STM32) and should be playable.

## Mac / Windows / Linux Platform

For our PC builds, we want to focus on quickly providing a usable minimal
platform layer since it's not our primary platform. As such we'll use the
[SDL][sdl] library which abstracts away the gnarly details of interacting with
input/graphics/audio hardware on Mac/Windows/Linux. SDL provides access to both
keyboard and gamepad devices so we'll check for and translate both into our
input struct (allowing the game to be played with either). Since our target
display will likely be lower resolution than a typical PC monitor, we'll
present a 160x120 imagebuffer to the game code, then scale the image 2x-4x for
display on the PC (or just support a full-screen mode).

## STM32 Platform

The STM32 platform layer is a little more complex since there's no SDL-like
library to provide nice abstractions over the hardware components. Thankfully,
ST provides a library named [STM32Cube][stm32cube] which provides low-level
drivers for the peripherals on all their microcontrollers. It also includes a
Board Support Package ([BSP][bsp]) for the dev board we're using to quickly
configure the hardware for the specific components used on the board (eg the
480x272 LCD).

While the BSP won't help once we build a custom board (since we'll likely not
have exactly the same external components as on the dev board), it gives us a
great way to bootstrap development on the dev board.

# Concerns / Open Questions

The STM32 has a couple of specialised hardware peripherals that may
significantly improve our games performance on that platform. For example it
has a 2D-aware [DMA][dma] engine, meaning it can copy 2D image data very
quickly without blocking the CPU. Our current platform API provides no way of
exposing such functionality. More generally the platform code provides no way
to expose platform functions to the game - currently platform always calls
game, never the other way round. We may want to expose a "Platform API" to the
game such that it can call platform methods (eg `platform.fast_image_copy()`).

For the STM32 we haven't defined where the game vs platform code live. Are they
linked together so each game includes it's own version of the platform code. Or
does the platform layer include a "home screen" game loader which lets the user
select different game binaries.

[gameloop]: http://gameprogrammingpatterns.com/game-loop.html
[opengl]: https://www.khronos.org/registry/OpenGL-Refpages/es2.0/
[chiptune]: https://en.wikipedia.org/wiki/Chiptune
[sdl]: https://www.libsdl.org/index.php
[bsp]: https://en.wikipedia.org/wiki/Board_support_package
[stm32cube]: https://www.st.com/en/embedded-software/stm32cubef7.html
[dma]: https://en.wikipedia.org/wiki/Direct_memory_access
