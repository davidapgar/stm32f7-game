# AS OF YET UNNAMED GAME CONSOLE THING

This project aims to build a 2D game engine for a small microcontroller as well as an associated piece of (Gameboy Advance-like) hardware on which to run games. The primary goal is to serve as an educational piece of software & hardware.

![example game gif](https://bitbucket.org/rng/stm32f7-game/raw/master/doc/images/readme-example-game.gif)

## Status

The engine is currently under active development and APIs are expected to change. There is sufficient infrastructure to build a simple (minus audio) game, runnable on PC and the [STM32F746G Discovery][stm32disco] dev board.

For a more detailed overview of the current status of the hardware and software, see the [status][status] page.

## Getting Started

The game should build and run on Linux (tested on Ubuntu 17.04), Mac OS X (tested on 10.12). Both platforms are also capable of building/flashing the embedded (STM32) build. Windows is currently not supported but should be relatively simple to add.

### Setup

    git submodule update --init --recursive

    # Ubuntu dependencies
    sudo add-apt-repository ppa:team-gcc-arm-embedded/ppa
    sudo apt-get update
    sudo apt-get install build-essential gdb libsdl2-dev gcc-arm-embedded openocd ptyhon3-pillow

### Build / Run / Debug

    # Build and run on PC
    make run

    # Build and debug on PC
    make debug

    # Build and run / debug on STM32
    make PLAT=stm debugserver &
    make PLAT=stm debug

### Repo Layout

This repo contains the source for the engine, platform code for PC and STM32, and an example game. The code is structured as follows:

    .
    ├── doc            # RFCs and other design documentation
    ├── example        # Example game
    └── src
        ├── engine     # Core engine 
        ├── lib        # 3rd party vendor code
        ├── plat_sdl   # Platform layer for SDL (PC)
        └── plat_stm   # Platform layer for STM32 (ARM)

The intent (though not currently implemented) is for game developers to submodule this repo to be able to quickly start developing new games.

## Contributing

One of the project goals is to make the process of building the console (hardware & software) well-documented as an educational resource. As such, we have an RFC process for big hardware and software decisions/changes. While this does increase the overhead of committing changes, the hope is that it provides an educational record for future readers of the project.

See the [Project Overview][overview] for more details on the RFC process and how to contribute, and the [Todo][todo] page for list of suggested starting points.


[stm32disco]: https://www.st.com/en/evaluation-tools/32f746gdiscovery.html
[status]: https://bitbucket.org/rng/stm32f7-game/src/master/doc/status.md
[overview]: https://bitbucket.org/rng/stm32f7-game/src/master/doc/rfc-0001-project-overview.md
[todo]: https://bitbucket.org/rng/stm32f7-game/src/master/doc/todo.md
